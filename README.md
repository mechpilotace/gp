# Games Played
Keeping track of games played and beaten as a way to make myself finish them. 
## Septmeber 2020
* The Legend of Zelda: Breath of the Wild - Nintendo Switch - September 2020 ? [Completed]
## October 2020
* Paper Mario: Origami King - Nintendo Switch - October 2020 [Completed]
## November 2020
* Paper Mario - Nintendo 64 - [In Progress]
* Super Mario 64 - Nintendo Switch 3D-All Stars - [In Progress]
* Doom 3 - PC - [In Progress]
* Demon's Souls - PS3 - [Restart]
* Resident Evil 5 - PS3 - [Started Co-Op with Joe]
* Diablo - PC - [Started Co-Op with Bobby]

#### To Play List
* VA-11 Hall-A - Nintendo Switch or PC
* Final Fantasy Tactics Advanced - GBA - [Restart]
* Final Fantasy Tactics A2 - NDS
* Tactics Ogre - PSP ? 
* Lunar: Silver Star Story Complete - PSP / Sega CD 
* Pokemon Fire Red - GBA
* Advance Wars - GBA
* Advanced Wars 2 - GBA
* Advanced Wars DS - NDS
* Paper Mario: Thousand Year Door - Gamecube
* Witcher 2 - PC
* Witcher 3 - PC
* Cyberpunk 2077 - PC [Not Released yet]
* Torchlight 1
* Torchlight 2 
